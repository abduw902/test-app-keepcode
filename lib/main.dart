import 'package:flutter/material.dart';
import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/core/network_utils/handshaking_fix.dart';
import 'package:test_app_keepcode/dp.dart';
import 'package:test_app_keepcode/pages/home/presentation/home.dart';

Future<void> main() async {
  await AppGlobalState.init();
  dioClientAdapterFixHandshaking();
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return DependencyProvider(
      child: MaterialApp(
        title: 'Keepcode',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Home(),
      ),
    );
  }
}
