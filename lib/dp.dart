import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/core/cubits/auth/auth_cubit.dart';
import 'package:test_app_keepcode/core/data/repositories/auth_repository.dart';
import 'package:test_app_keepcode/pages/home/cubits/buy_number/buy_number_cubit.dart';
import 'package:test_app_keepcode/pages/home/cubits/delete_number/delete_number_cubit.dart';
import 'package:test_app_keepcode/pages/home/cubits/services/services_cubit.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/buy_number_repository.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/delete_number_repository.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/services_repository.dart';

class DependencyProvider extends StatelessWidget {
  final Widget child;

  const DependencyProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => AuthCubit(
            AuthRepositoryImpl(),
          ),
        ),
        BlocProvider(
          create: (BuildContext context) => ServicesCubit(
            ServicesRepositoryImplements(),
          ),
        ),
        BlocProvider(
          create: (BuildContext context) => BuyNumberCubit(
            BuyNumberRepositoryImpl(),
          ),
        ),
        BlocProvider(
          create: (BuildContext context) => DeleteNumberCubit(
            DeleteNumberRepositoryImpl(),
          ),
        ),
      ],
      child: child,
    );
  }
}
