part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthLoading extends AuthState {
  final bool refreshSession;

  const AuthLoading(this.refreshSession);

  @override
  List<Object?> get props => [refreshSession];
}

class AuthLoaded extends AuthState {
  final UserModel userModel;
  final bool refreshSession;

  const AuthLoaded(this.userModel, this.refreshSession);

  @override
  List<Object?> get props => [userModel, refreshSession];
}

class AuthError extends AuthState {
  final String error;
  final bool refreshSession;

  const AuthError(this.error, this.refreshSession);

  @override
  List<Object?> get props => [error, refreshSession];
}
