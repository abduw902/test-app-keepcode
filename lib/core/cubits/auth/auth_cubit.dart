import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/core/data/models/user_model.dart';
import 'package:test_app_keepcode/core/data/repositories/auth_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository authRepository;

  AuthCubit(this.authRepository) : super(AuthInitial());

  Future<void> auth({bool refreshSession = false}) async {
    try {
      emit(AuthLoading(refreshSession));
      final response = await authRepository.authorize();
      AppGlobalState.localStorage.saveUserModel(response);
      emit(AuthLoaded(response, refreshSession));
    } catch (e) {
      emit(AuthError(e.toString(), refreshSession));
    }
  }
}
