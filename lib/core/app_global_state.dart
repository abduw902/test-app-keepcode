import 'package:hive_flutter/adapters.dart';
import 'package:test_app_keepcode/core/constants/storage_constants.dart';
import 'package:test_app_keepcode/core/data/data_sourses/local_storage.dart';
import 'package:test_app_keepcode/core/data/models/user_model.dart';
import 'package:test_app_keepcode/pages/home/data/models/numbers.dart';

class AppGlobalState {
  static late LocalStorage localStorage;

  static Future<void> init() async {
    await Hive.initFlutter();
    localStorage = LocalStorageImplements();
    if (!Hive.isAdapterRegistered(0)) {
      Hive.registerAdapter(UserModelAdapter());
    }
    if (!Hive.isAdapterRegistered(1)) {
      Hive.registerAdapter((NumberAdapter()));
    }

    await Hive.openBox(StorageConstants.userBox);
    await Hive.openBox(StorageConstants.myNumbers);
  }
}
