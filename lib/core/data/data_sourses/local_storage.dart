import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:test_app_keepcode/core/constants/storage_constants.dart';
import 'package:test_app_keepcode/core/data/models/user_model.dart';
import 'package:test_app_keepcode/pages/home/data/models/numbers.dart';

abstract class LocalStorage {
  UserModel getUserModel();

  void saveUserModel(UserModel userModel);

  List<Number> getMyNumbers();

  void saveMyNumbers(List<Number> services);
}

class LocalStorageImplements implements LocalStorage {
  @override
  List<Number> getMyNumbers() {
    final box = Hive.box(StorageConstants.myNumbers);
    final res =
        jsonEncode(box.get(StorageConstants.myNumbers, defaultValue: []));
    return (json.decode(res) as List)
        .map((data) => Number.fromJson(data))
        .toList();
  }

  @override
  UserModel getUserModel() {
    final box = Hive.box(StorageConstants.userBox);
    return box.get(StorageConstants.userBox);
  }

  @override
  void saveMyNumbers(List<Number> services) {
    final box = Hive.box(StorageConstants.myNumbers);
    box.put(StorageConstants.myNumbers, services);
  }

  @override
  void saveUserModel(UserModel userModel) {
    final box = Hive.box(StorageConstants.userBox);
    box.put(StorageConstants.userBox, userModel);
  }
}
