import 'dart:convert';

import 'package:test_app_keepcode/core/data/models/user_model.dart';
import 'package:test_app_keepcode/core/network_utils/dio_client.dart';

abstract class AuthRepository {
  Future<UserModel> authorize();
}

class AuthRepositoryImpl implements AuthRepository {
  @override
  Future<UserModel> authorize() async {
    try {
      final response = await DioClient.getRequest(
          "/handler_auth.php?owner=6&email=adwtrafanet@yandex.ru&code=111111&action=confirmEmailByCode");
      return UserModel.fromJson(jsonDecode(response.data));
    } catch (e) {
      rethrow;
    }
  }
}
