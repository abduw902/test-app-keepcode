import 'package:hive/hive.dart';

part 'user_model.g.dart';

@HiveType(typeId: 0)
class UserModel {
  @HiveField(0)
  String sessionId;
  @HiveField(1)
  String? balance;
  @HiveField(2)
  String? cashback;
  @HiveField(3)
  String refreshToken;
  @HiveField(4)
  int userid;
  @HiveField(5)
  String? email;
  @HiveField(6)
  String? telegramId;

  UserModel({
    required this.sessionId,
    this.balance,
    this.cashback,
    required this.refreshToken,
    required this.userid,
    this.email,
    this.telegramId,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        sessionId: json["sessionId"],
        balance: json["balance"],
        cashback: json["cashback"],
        refreshToken: json["refresh_token"],
        userid: json["userid"],
        email: json["email"],
        telegramId: json["telegram_id"],
      );

  Map<String, dynamic> toJson() => {
        "sessionId": sessionId,
        "balance": balance,
        "cashback": cashback,
        "refresh_token": refreshToken,
        "userid": userid,
        "email": email,
        "telegram_id": telegramId,
      };
}
