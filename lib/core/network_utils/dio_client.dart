import 'package:dio/dio.dart';
import 'package:test_app_keepcode/core/constants/api.dart';

class DioClient {
  static Dio dio = Dio(BaseOptions(
      baseUrl: API.URL,
      connectTimeout: const Duration(milliseconds: 20000000),
      receiveTimeout: const Duration(milliseconds: 20000000),
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      responseType: ResponseType.json))
    ..interceptors.add(logInterceptor);

  static final logInterceptor = LogInterceptor(
    request: true,
    responseBody: true,
    requestBody: true,
    requestHeader: true,
  );

  static Future<Response> getRequest(String url) async => await dio.get(url);

  static Future<Response> postRequest(String url, {required data}) async =>
      await dio.post(url, data: data);

  static Future<Response> putRequest(String url, {data}) async =>
      await dio.put(url, data: data);

  static Future<Response> deleteRequest(String url) async =>
      await dio.delete(url);

  static Future uploadRequest(String url, String filePath) async {
    dio.options.contentType = "multipart/form-data";
    final multiPartFile = await MultipartFile.fromFile(
      filePath,
      filename: filePath.split('/').last,
    );
    FormData formData = FormData.fromMap({
      "file": multiPartFile,
    });
    final response = await dio.post(
      url,
      data: formData,
    );

    return response.data;
  }
}
