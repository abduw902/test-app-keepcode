import 'dart:io';
import 'package:dio/io.dart';
import 'package:test_app_keepcode/core/network_utils/dio_client.dart';

///DIO CLIENT SOME EDITIONS TO FIX HANDSHAKING ERROR.
///THIS METHOD SHOULD BE CALLED AFTER JUST DIO INSTANCED
void dioClientAdapterFixHandshaking() {
  (DioClient.dio.httpClientAdapter as IOHttpClientAdapter).onHttpClientCreate =
      (client) {
    client.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    return client;
  };
}
