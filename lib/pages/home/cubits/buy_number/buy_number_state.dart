part of 'buy_number_cubit.dart';

abstract class BuyNumberState extends Equatable {
  const BuyNumberState();
}

class BuyNumberInitial extends BuyNumberState {
  @override
  List<Object> get props => [];
}

class Buying extends BuyNumberState {
  @override
  List<Object?> get props => [];
}

class Bought extends BuyNumberState {
  @override
  List<Object?> get props => [];
}

class BuyingError extends BuyNumberState {
  final String error;

  const BuyingError(this.error);

  @override
  List<Object?> get props => [error];
}
