import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/buy_number_repository.dart';

part 'buy_number_state.dart';

class BuyNumberCubit extends Cubit<BuyNumberState> {
  final BuyNumberRepository repository;

  BuyNumberCubit(this.repository) : super(BuyNumberInitial());

  Future<void> buyNumber(String id) async {
    try {
      emit(Buying());
      final response = await repository.buyNumber(id);
      if (response.error != null) {
        emit(BuyingError(response.error!));
      } else {
        final myNumbers = AppGlobalState.localStorage.getMyNumbers();
        myNumbers.add(response);
        AppGlobalState.localStorage.saveMyNumbers(myNumbers);
        emit(Bought());
      }
    } catch (e) {
      emit(BuyingError(e.toString()));
    }
  }
}
