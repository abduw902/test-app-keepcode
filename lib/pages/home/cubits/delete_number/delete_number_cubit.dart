import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/delete_number_repository.dart';

part 'delete_number_state.dart';

class DeleteNumberCubit extends Cubit<DeleteNumberState> {
  final DeleteNumberRepository repository;

  DeleteNumberCubit(this.repository) : super(DeleteNumberInitial());

  Future<void> deleteNumber(String id) async {
    try {
      emit(Deleting());
      await repository.deleteNumber(id);
      final myNumbers = AppGlobalState.localStorage.getMyNumbers();
      myNumbers.removeWhere((element) => element.activation == id);
      AppGlobalState.localStorage.saveMyNumbers(myNumbers);
      emit(Deleted());
    } catch (e) {
      emit(DeleteError(e.toString()));
    }
  }
}
