part of 'delete_number_cubit.dart';

abstract class DeleteNumberState extends Equatable {
  const DeleteNumberState();
}

class DeleteNumberInitial extends DeleteNumberState {
  @override
  List<Object> get props => [];
}

class Deleting extends DeleteNumberState {
  @override
  List<Object?> get props => [];
}

class Deleted extends DeleteNumberState {
  @override
  List<Object?> get props => [];
}

class DeleteError extends DeleteNumberState {
  final String error;

  const DeleteError(this.error);

  @override
  List<Object?> get props => [error];
}
