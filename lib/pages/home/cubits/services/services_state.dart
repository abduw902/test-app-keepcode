part of 'services_cubit.dart';

abstract class ServicesState extends Equatable {
  const ServicesState();
}

class ServicesInitial extends ServicesState {
  @override
  List<Object> get props => [];
}

class ServicesLoading extends ServicesState {
  @override
  List<Object?> get props => [];
}

class ServicesLoaded extends ServicesState {
  final List<SortedServices> services;

  const ServicesLoaded(this.services);

  @override
  List<Object?> get props => [services];
}

class ServicesError extends ServicesState {
  final String error;

  const ServicesError(this.error);

  @override
  List<Object?> get props => [error];
}
