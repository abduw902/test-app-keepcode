import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/pages/home/data/models/sorted_services.dart';
import 'package:test_app_keepcode/pages/home/data/repositories/services_repository.dart';

part 'services_state.dart';

class ServicesCubit extends Cubit<ServicesState> {
  final ServicesRepository servicesRepository;

  ServicesCubit(this.servicesRepository) : super(ServicesInitial());

  Future<void> getServices() async {
    final List<SortedServices> services = [];
    try {
      emit(ServicesLoading());
      final servicesWithLocalization =
          await servicesRepository.getServicesWithLocalization();
      final servicesFromBE = await servicesRepository.getServices();
      if (servicesFromBE.isNotEmpty &&
          servicesWithLocalization.services.isNotEmpty) {
        for (int i = 0; i < servicesFromBE.length; i++) {
          final serviceFromMap = servicesWithLocalization.services.entries
              .where((element) =>
                  element.key.split("_").first == servicesFromBE[i].shortName)
              .first
              .value;
          services.add(SortedServices(
            shortName: servicesFromBE[i].shortName,
            sellTop: servicesFromBE[i].sellTop,
            totalCount: servicesFromBE[i].totalCount,
            minPrice: servicesFromBE[i].minPrice,
            en: serviceFromMap.en,
            id: serviceFromMap.id,
          ));
        }
      }
      emit(ServicesLoaded(services));
    } catch (e) {
      emit(ServicesError(e.toString()));
    }
  }
}
