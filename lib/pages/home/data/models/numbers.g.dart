// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'numbers.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class NumberAdapter extends TypeAdapter<Number> {
  @override
  final int typeId = 1;

  @override
  Number read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Number(
      activation: fields[0] as String?,
      phone: fields[1] as String?,
      timeStart: fields[2] as int?,
      timeEnd: fields[3] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, Number obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.activation)
      ..writeByte(1)
      ..write(obj.phone)
      ..writeByte(2)
      ..write(obj.timeStart)
      ..writeByte(3)
      ..write(obj.timeEnd);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is NumberAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
