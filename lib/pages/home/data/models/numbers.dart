import 'dart:convert';

import 'package:hive_flutter/hive_flutter.dart';

part 'numbers.g.dart';

Number numberFromJson(String str) => Number.fromJson(json.decode(str));

String numberToJson(Number data) => json.encode(data.toJson());

@HiveType(typeId: 1)
class Number {
  String? status;
  String? error;
  @HiveField(0)
  String? activation;
  @HiveField(1)
  String? phone;
  @HiveField(2)
  int? timeStart;
  @HiveField(3)
  int? timeEnd;

  Number({
    this.status,
    this.error,
    this.activation,
    this.phone,
    this.timeStart,
    this.timeEnd,
  });

  factory Number.fromJson(Map<String, dynamic> json) => Number(
        status: json["status"],
        error: json["error"],
        activation: json["activation"],
        phone: json["phone"],
        timeStart: json["timeStart"],
        timeEnd: json["timeEnd"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "activation": activation,
        "phone": phone,
        "timeStart": timeStart,
        "timeEnd": timeEnd,
      };
}
