class SortedServices {
  String? shortName;
  int? sellTop;
  int? totalCount;
  double? minPrice;
  String? en;
  String? id;

  SortedServices({
    this.shortName,
    this.sellTop,
    this.totalCount,
    this.minPrice,
    this.en,
    this.id,
  });

  factory SortedServices.fromJson(Map<String, dynamic> json) => SortedServices(
        shortName: json["shortName"],
        sellTop: json["sellTop"],
        totalCount: json["totalCount"],
        minPrice: json["minPrice"]?.toDouble(),
        en: json["en"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "shortName": shortName,
        "sellTop": sellTop,
        "totalCount": totalCount,
        "minPrice": minPrice,
        "en": en,
        "id": id,
      };
}
