class Services {
  String? shortName;
  int? sellTop;
  bool? forward;
  int? totalCount;
  double? minPrice;
  double? minFreePrice;
  int? countWithFreePrice;
  bool? onlyRent;

  Services({
    this.shortName,
    this.sellTop,
    this.forward,
    this.totalCount,
    this.minPrice,
    this.minFreePrice,
    this.countWithFreePrice,
    this.onlyRent,
  });

  factory Services.fromJson(Map<String, dynamic> json) => Services(
        shortName: json["shortName"],
        sellTop: json["sellTop"],
        forward: json["forward"],
        totalCount: json["totalCount"],
        minPrice: json["minPrice"]?.toDouble(),
        minFreePrice: json["minFreePrice"]?.toDouble(),
        countWithFreePrice: json["countWithFreePrice"],
        onlyRent: json["onlyRent"],
      );

  Map<String, dynamic> toJson() => {
        "shortName": shortName,
        "sellTop": sellTop,
        "forward": forward,
        "totalCount": totalCount,
        "minPrice": minPrice,
        "minFreePrice": minFreePrice,
        "countWithFreePrice": countWithFreePrice,
        "onlyRent": onlyRent,
      };
}
