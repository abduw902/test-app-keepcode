class ServicesWithLocalization {
  Map<String, Service> services;

  ServicesWithLocalization({
    required this.services,
  });

  factory ServicesWithLocalization.fromJson(Map<String, dynamic> json) {
    final Map<String, dynamic> servicesJson = json['services'];
    final services = servicesJson
        .map((key, value) => MapEntry(key, Service.fromJson(value)));
    return ServicesWithLocalization(services: services);
  }

  Map<String, dynamic> toJson() => {
        "services": Map.from(services)
            .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
      };
}

class Service {
  String? ru;
  String? en;
  String? keywords;
  String? id;
  String? sellTop;

  Service({
    this.ru,
    this.en,
    this.keywords,
    this.id,
    this.sellTop,
  });

  factory Service.fromJson(Map<String, dynamic> json) => Service(
        ru: json["ru"],
        en: json["en"],
        keywords: json["keywords"],
        id: json["id"],
        sellTop: json["sellTop"],
      );

  Map<String, dynamic> toJson() => {
        "ru": ru,
        "en": en,
        "keywords": keywords,
        "id": id,
        "sellTop": sellTop,
      };
}
