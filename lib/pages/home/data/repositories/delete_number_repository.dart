import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/core/network_utils/dio_client.dart';

abstract class DeleteNumberRepository {
  Future<void> deleteNumber(String id);
}

class DeleteNumberRepositoryImpl implements DeleteNumberRepository {
  @override
  Future<void> deleteNumber(String id) async {
    final refreshToken =
        AppGlobalState.localStorage.getUserModel().refreshToken;
    final userId = AppGlobalState.localStorage.getUserModel().userid;
    final sessionId = AppGlobalState.localStorage.getUserModel().sessionId;
    await DioClient.getRequest(
        "https://sms-activate.org/stubs/handler_api.php?refresh_token=$refreshToken&sessionId=$sessionId&owner=6&userid=$userId&action=setStatus&id=$id&status=8");
  }
}
