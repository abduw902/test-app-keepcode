import 'dart:convert';

import 'package:test_app_keepcode/core/network_utils/dio_client.dart';
import 'package:test_app_keepcode/pages/home/data/models/services.dart';
import 'package:test_app_keepcode/pages/home/data/models/services_with_localization.dart';

abstract class ServicesRepository {
  Future<ServicesWithLocalization> getServicesWithLocalization();

  Future<List<Services>> getServices();
}

class ServicesRepositoryImplements implements ServicesRepository {
  @override
  Future<List<Services>> getServices() async {
    final res = await DioClient.getRequest(
        "/apiMobile.php?owner=6&action=getAllServices");
    return (jsonDecode(res.data) as List).map((e) => Services.fromJson(e)).toList();
  }

  @override
  Future<ServicesWithLocalization> getServicesWithLocalization() async {
    final res = await DioClient.getRequest(
        "/apiMobile.php?owner=6&action=getAllServicesAndAllCountries");
    return ServicesWithLocalization.fromJson(jsonDecode(res.data));
  }
}
