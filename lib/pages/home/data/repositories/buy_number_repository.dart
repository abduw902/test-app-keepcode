import 'dart:convert';

import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/core/network_utils/dio_client.dart';
import 'package:test_app_keepcode/pages/home/data/models/numbers.dart';

abstract class BuyNumberRepository {
  Future<Number> buyNumber(String id);
}

class BuyNumberRepositoryImpl implements BuyNumberRepository {
  @override
  Future<Number> buyNumber(String id) async {
    final refreshToken =
        AppGlobalState.localStorage.getUserModel().refreshToken;
    final userId = AppGlobalState.localStorage.getUserModel().userid;
    final sessionId = AppGlobalState.localStorage.getUserModel().sessionId;
    final res = await DioClient.getRequest(
        "https://sms-activate.org/stubs/handler_api.php?refresh_token=$refreshToken&sessionId=$sessionId&owner=6&userid=$userId&country=0&service=$id&action=getNumberMobile&forward=0&operator=any");
    return Number.fromJson(jsonDecode(res.data));
  }
}
