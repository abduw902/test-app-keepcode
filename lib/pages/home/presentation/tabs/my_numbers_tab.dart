import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/core/app_global_state.dart';
import 'package:test_app_keepcode/core/utils/toast_utils.dart';
import 'package:test_app_keepcode/pages/home/cubits/delete_number/delete_number_cubit.dart';
import 'package:test_app_keepcode/pages/home/presentation/widgets/number_widget.dart';

class MyNumbersTab extends StatelessWidget {
  const MyNumbersTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DeleteNumberCubit, DeleteNumberState>(
      listener: (context, state) {
        if (state is DeleteError) {
          ToastUtils.showShortToast(context, state.error);
        }
      },
      builder: (context, state) {
        return ListView.builder(
          itemBuilder: (context, index) {
            return NumberWidget(
                number: AppGlobalState.localStorage.getMyNumbers()[index],
                onDelete: () {
                  BlocProvider.of<DeleteNumberCubit>(context).deleteNumber(
                    AppGlobalState.localStorage
                        .getMyNumbers()[index]
                        .activation!,
                  );
                });
          },
          itemCount: AppGlobalState.localStorage.getMyNumbers().length,
        );
      },
    );
  }
}
