import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/pages/home/cubits/buy_number/buy_number_cubit.dart';
import 'package:test_app_keepcode/pages/home/cubits/services/services_cubit.dart';
import 'package:test_app_keepcode/pages/home/presentation/widgets/service_widget.dart';

class ServicesTab extends StatefulWidget {
  const ServicesTab({Key? key}) : super(key: key);

  @override
  State<ServicesTab> createState() => _ServicesTabState();
}

class _ServicesTabState extends State<ServicesTab> {
  @override
  void initState() {
    BlocProvider.of<ServicesCubit>(context).getServices();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ServicesCubit, ServicesState>(
      builder: (context, state) {
        if (state is ServicesLoaded) {
          final services = state.services;

          return ListView.builder(
              itemBuilder: (context, index) {
                return ServiceWidget(
                  sortedService: services[index],
                  onBuy: () {
                    BlocProvider.of<BuyNumberCubit>(context)
                        .buyNumber(services[index].shortName!);
                  },
                );
              },
              itemCount: services.length);
        }
        return const Center(
          child: CircularProgressIndicator(
            color: Colors.white,
          ),
        );
      },
    );
  }
}
