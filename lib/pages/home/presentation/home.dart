import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_keepcode/core/cubits/auth/auth_cubit.dart';
import 'package:test_app_keepcode/core/utils/toast_utils.dart';
import 'package:test_app_keepcode/pages/home/cubits/buy_number/buy_number_cubit.dart';
import 'package:test_app_keepcode/pages/home/presentation/tabs/my_numbers_tab.dart';
import 'package:test_app_keepcode/pages/home/presentation/tabs/services_tab.dart';
import 'package:test_app_keepcode/pages/home/presentation/widgets/keep_page_alive.dart';
import 'package:test_app_keepcode/pages/home/presentation/widgets/simple_tab_bar.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  late final TabController tabController;
  late final PageController pageController;

  @override
  void initState() {
    tabController = TabController(vsync: this, length: 2, initialIndex: 0);
    pageController = PageController(initialPage: 0);
    BlocProvider.of<AuthCubit>(context).auth();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Center(child: Text('Keepcode')),
      ),
      backgroundColor: Colors.blue,
      body: BlocConsumer<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is AuthLoaded && !state.refreshSession) {
            Timer.periodic(const Duration(minutes: 55), (timer) {
              BlocProvider.of<AuthCubit>(context).auth(refreshSession: true);
            });
          }
          if (state is AuthError) {
            ToastUtils.showShortToast(context, state.error);
            BlocProvider.of<AuthCubit>(context)
                .auth(refreshSession: state.refreshSession);
          }
        },
        builder: (context, state) {
          if (state is AuthLoading && !state.refreshSession) {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.white,
              ),
            );
          }
          return Column(
            children: [
              SimpleTabBar(
                  tabTitles: const ["Services", "My numbers"],
                  tabController: tabController,
                  onTabTapped: onTabTapped),
              Flexible(
                child: BlocListener<BuyNumberCubit, BuyNumberState>(
                  listener: (context, state) {
                    if (state is BuyingError) {
                      ToastUtils.showShortToast(context, state.error);
                    }
                    if (state is Bought) {
                      ToastUtils.showShortToast(context, "bought");
                      onTabTapped(1);
                    }
                  },
                  child: PageView(
                    controller: pageController,
                    onPageChanged: onPageChanged,
                    children: const [
                      KeepPageAlive(child: ServicesTab()),
                      MyNumbersTab(),
                    ],
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    pageController.dispose();
    super.dispose();
  }

  void onPageChanged(int page) {
    tabController.index = page;
  }

  void onTabTapped(int tab) {
    pageController.jumpToPage(tab);
  }
}
