import 'package:flutter/material.dart';

class SimpleTabBar extends StatelessWidget {
  final List<String> tabTitles;
  final TabController tabController;
  final Function(int) onTabTapped;
  final Color? backgroundColor;
  final Color? textColor;
  final bool isScrollable;
  final EdgeInsets? padding;

  const SimpleTabBar({
    Key? key,
    required this.tabTitles,
    required this.tabController,
    required this.onTabTapped,
    this.backgroundColor,
    this.textColor,
    this.isScrollable = false,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: backgroundColor ?? Theme.of(context).primaryColor,
      width: double.infinity,
      child: TabBar(
        isScrollable: isScrollable,
        controller: tabController,
        labelColor: textColor ?? Colors.white,
        indicatorColor: Colors.yellow,
        labelPadding: const EdgeInsets.symmetric(horizontal: 2),
        indicatorSize: TabBarIndicatorSize.label,
        labelStyle: Theme.of(context).textTheme.bodyText1,
        unselectedLabelColor: textColor != null
            ? textColor!.withOpacity(0.7)
            : Colors.white.withOpacity(0.4),
        onTap: onTabTapped,
        tabs: [
          for (int i = 0; i < tabTitles.length; i++)
            Padding(
              padding: padding ?? EdgeInsets.zero,
              child: Tab(text: tabTitles[i]),
            ),
        ],
      ),
    );
  }
}
