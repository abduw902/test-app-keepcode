import 'package:flutter/material.dart';
import 'package:test_app_keepcode/pages/home/data/models/sorted_services.dart';

class ServiceWidget extends StatelessWidget {
  final SortedServices sortedService;
  final VoidCallback onBuy;

  const ServiceWidget(
      {Key? key, required this.sortedService, required this.onBuy})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: Colors.white),
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Row(
                children: [
                  SizedBox(
                    height: 50,
                    width: 50,
                    child: ClipRRect(
                      child: Image.network(
                        "https://smsactivate.s3.eu-central-1.amazonaws.com/assets/ico/${sortedService.shortName}0.webp",
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("name:"),
                            Expanded(
                              child: Text(
                                sortedService.en ?? "",
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.end,
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("numbers:"),
                            Expanded(
                                child: Text(
                              sortedService.totalCount.toString(),
                              overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.end,
                            ))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("price:"),
                            Text(sortedService.minPrice.toString())
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              flex: 1,
              child: GestureDetector(
                onTap: onBuy,
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.blue),
                  child: const Center(
                    child: Text(
                      "buy",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
